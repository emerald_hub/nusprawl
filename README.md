# NuSprawl

Welcome to NuSprawl, the Shadowrun 5th edition MUD. 

# Installation
NuSprawl uses the Evennia codebase which has a particular, but straightforward installation process. Below are the steps for creating a running installation for NuSprawl locally.
* Clone the NuSprawl repository
  * `git@gitlab.com:emerald_hub/nusprawl.git`
* Clone the Evennia project into the repository
  * `cd nusprawl`
  * `git clone https://github.com/evennia/evennia.git`
* Create a Python virtual environment within the new directory:
  * `virtualenv venv`
* Activate the virtual environment
  * `source venv/bin/activate`
* Install Evennia
  * `pip install -e evennia`
  * `cd nusprawl`
  * `evennia migrate`
* Start NuSprawl
  * `evennia start`