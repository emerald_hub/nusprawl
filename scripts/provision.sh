#!/usr/bin/env bash

sudo apt-get update -qqq
sudo apt-get upgrade -y -qqq
sudo apt-get install -y git gcc python3 python3-dev python3-venv python3-pip -qqq

cd /opt/nusprawl
git clone -b develop-py3 https://github.com/evennia/evennia.git
python3 -m venv venv
source venv/bin/activate
pip install -e evennia -qqq
cp /vagrant/provision/evennia.db3 /opt/nusprawl/nusprawl/server/evennia.db3
cp /vagrant/provision/secret_settings.py /opt/nusprawl/nusprawl/server/conf/secret_settings.py
cd nusprawl
evennia makemigrations
evennia migrate
